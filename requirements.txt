lxml==4.9.3
openpyxl==3.1.2
pandas==2.1.3
Pillow==10.1.0
requests==2.31.0
ultralytics==8.0.222